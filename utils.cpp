#include "stdafx.h"
#include "utils.h"

//************************************
// Method:    change_charset				字符编码转换
// FullName:  change_charset
// Access:    public 
// Returns:   std::string					转换后的字符串
// Qualifier:
// Parameter: const std::string & str		转换前的字符串
// Parameter: UINT from_charset				原字符串编码
// Parameter: UINT to_charset				转换后的编码
//************************************
std::string change_charset(const std::string &str, UINT from_charset, UINT to_charset)
{
	WCHAR *lpWideChar = new WCHAR[str.length() + 1];
	UINT nwLen = MultiByteToWideChar(from_charset, 0, str.c_str(), str.length()+1, NULL, 0);
	MultiByteToWideChar(from_charset, 0, str.c_str(), str.length()+1, lpWideChar, nwLen);
	nwLen = WideCharToMultiByte(to_charset, 0, lpWideChar, -1, NULL, 0, NULL, FALSE);
	char *pChar = new char[nwLen + 1];
	WideCharToMultiByte(to_charset, 0, lpWideChar, -1, pChar, nwLen, NULL, NULL);
	std::string result(pChar);
	delete[] pChar;
	delete[] lpWideChar;
	return result;
}

//************************************
// Method:    string_to_wide_char			string对象转换为Windows字符串对象
// FullName:  string_to_wide_char
// Access:    public 
// Returns:   LPCTSTR						转换后的Windows字符串对象（需要自行delete）
// Qualifier:
// Parameter: const std::string & str		string对象
//************************************
LPCTSTR string_to_wide_char(const std::string &str)
{
	if (str.length() > BUFFER_SIZE)
	{
		return NULL;
	}
	static WCHAR lpWideChar[BUFFER_SIZE];
	UINT nwLen = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length()+1, NULL, 0);
	MultiByteToWideChar(CP_UTF8, 0, str.c_str(), str.length()+1, lpWideChar, nwLen);
	return lpWideChar;
}

//************************************
// Method:    wide_char_to_string			Windows字符串对象转换为string对象
// FullName:  wide_char_to_string
// Access:    public 
// Returns:   std::string					string对象
// Qualifier:
// Parameter: LPCTSTR str					Windows字符串对象
//************************************
std::string wide_char_to_string(LPCTSTR str)
{
	UINT nwLen = WideCharToMultiByte(CP_UTF8, 0, str, -1, NULL, 0, NULL, FALSE);
	char *pChar = new char[nwLen + 1];
	WideCharToMultiByte(CP_UTF8, 0, str, -1, pChar, nwLen, NULL, NULL);
	std::string result(pChar);
	delete[] pChar;
	return result;
}

//************************************
// Method:    file_exist						判断一个文件是否存在
// FullName:  file_exist
// Access:    public 
// Returns:   bool								文件存在返回true，否则返回false
// Qualifier:
// Parameter: const std::string & file_path		文件路径
//************************************
bool file_exist(const std::string& file_path)
{
	HANDLE file_handle = CreateFile(string_to_wide_char(file_path), GENERIC_READ, NULL, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (ERROR_FILE_NOT_FOUND == GetLastError())
	{
		CloseHandle(file_handle);
		return FALSE;
	}
	else
	{
		CloseHandle(file_handle);
		return TRUE;
	}
}


//************************************
// Method:    count_number						计算一个数字有多少位（10进制）
// FullName:  count_number
// Access:    public 
// Returns:   UINT								数字的位数
// Qualifier:
// Parameter: UINT number						要计算的数字
//************************************
UINT count_number(UINT number)
{
	int nb = 1;
	while ((number /= 10) != 0)
	{
		++nb;
	}
	return nb;
}

//************************************
// Method:    format_string									格式化字符串
// FullName:  format_string
// Access:    public 
// Returns:   std::string									格式化之后的字符串
// Qualifier:
// Parameter: const std::string & format_str				字符串格式
// Parameter: const std::vector<std::string> & value_str	要填入字符串的字符串
//************************************
std::string format_string(const std::string& format_str, const std::vector<std::string> &value_str)
{
	char buffer[10];
	unsigned int no = 0;
	std::string fmt;
	std::string result = format_str;
	for (no = 0; no < value_str.size(); ++no)
	{
		fmt = "{";
		_itoa_s(no, buffer, 10);
		fmt.append(buffer);
		fmt.push_back('}');
		size_t pos = result.find(fmt);
		if (pos != std::string::npos)
		{
			result.replace(pos, count_number(no) + 2, value_str[no]);
		}
	}
	return result;
}

//************************************
// Method:    split									根据一个字符分割字符串
// FullName:  split
// Access:    public 
// Returns:   std::vector<std::string> &			切分后的字符串向量
// Qualifier:
// Parameter: const std::string & s					要切分的字符串
// Parameter: char delim							用于切分的字符
// Parameter: std::vector<std::string> & elems		用于保存切分后字符串的向量
//************************************
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) 
{
	std::stringstream ss(s);
	std::string item;
	while(std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


//************************************
// Method:    split								用指定字符切分字符串
// FullName:  split
// Access:    public 
// Returns:   std::vector<std::string>			切分完成的字符串
// Qualifier:
// Parameter: const std::string & s				要切分的字符串
// Parameter: char delim						用于切分的字符
//************************************
std::vector<std::string> split(const std::string &s, char delim) 
{
	std::vector<std::string> elems;
	return split(s, delim, elems);
}

//************************************
// Method:    mkdir									根据路径创建文件夹
// FullName:  mkdir
// Access:    public 
// Returns:   bool									始终为true
// Qualifier:
// Parameter: const std::string & file_path			文件夹路径
//************************************
bool mkdir(const std::string &file_path)
{
	std::vector<std::string> path_splited = split(file_path, '\\');
	std::string partial_path = path_splited[0] + '\\';
	for (std::vector<std::string>::iterator it = path_splited.begin() + 1; it != path_splited.end(); ++it)
	{
		partial_path += *it + '\\';
		LPCTSTR lp_partial_path = string_to_wide_char(partial_path);
		CreateDirectory(lp_partial_path, NULL);
		delete[] lp_partial_path;
	}
	return true;
}

//************************************
// Method:    replace								字符串中用一个字符串替代另一个
// FullName:  replace
// Access:    public 
// Returns:   std::string							替换后的字符串
// Qualifier:
// Parameter: std::string str						原字符串
// Parameter: const std::string & from				被替换的字符串
// Parameter: const std::string & to				替换成为的字符串
//************************************
std::string replace(std::string str, const std::string &from, const std::string &to)
{
	if (from == to)
	{
		return str;
	}
	size_t pos;
	while ((pos = str.find(from)) != std::string::npos)
	{
		str.replace(pos, from.length(), to);
	}
	return str;
}