// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "resource.h"

#include <fstream>

#include "MainDlg.h"
using namespace std;

string CMainDlg::filePath = "";

CMainDlg::CMainDlg():url("http://smarthosts.googlecode.com/svn/trunk/hosts")
{
	urlHander = curl_easy_init();
}

CMainDlg::~CMainDlg()
{
	curl_easy_cleanup(urlHander);
}

BOOL CMainDlg::PreTranslateMessage(MSG* pMsg)
{
	return CWindow::IsDialogMessage(pMsg);
}

BOOL CMainDlg::OnIdle()
{
	return FALSE;
}

LRESULT CMainDlg::OnInitDialog(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// center the dialog on the screen
	CenterWindow();

	// set icons
	HICON hIcon = AtlLoadIconImage(IDR_MAINFRAME, LR_DEFAULTCOLOR, ::GetSystemMetrics(SM_CXICON), ::GetSystemMetrics(SM_CYICON));
	SetIcon(hIcon, TRUE);
	HICON hIconSmall = AtlLoadIconImage(IDR_MAINFRAME, LR_DEFAULTCOLOR, ::GetSystemMetrics(SM_CXSMICON), ::GetSystemMetrics(SM_CYSMICON));
	SetIcon(hIconSmall, FALSE);

	// register object for message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->AddMessageFilter(this);
	pLoop->AddIdleHandler(this);

	UIAddChildWindowContainer(m_hWnd);

	// bind controls to objects
	mEditFilePath = GetDlgItem(IDC_EDIT_FILE_PATH);
	mBtnClear = GetDlgItem(IDC_BUTTON_CLEAR);
	mBtnUpdate = GetDlgItem(IDC_BUTTON_UPDATE);

	return TRUE;
}

LRESULT CMainDlg::OnDestroy(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
	// unregister message filtering and idle updates
	CMessageLoop* pLoop = _Module.GetMessageLoop();
	ATLASSERT(pLoop != NULL);
	pLoop->RemoveMessageFilter(this);
	pLoop->RemoveIdleHandler(this);
	return 0;
}

LRESULT CMainDlg::OnClose(UINT , WPARAM , LPARAM , BOOL& )
{
	DestroyWindow();
	PostQuitMessage(0);
	return 0;
}

LRESULT CMainDlg::OnBnClickedButtonBrowse(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CFileDialog fileDialog(true, _T("hosts"), _T("hosts"), NULL, _T("hosts文件(hosts)\0hosts"), NULL);
	if (IDOK == fileDialog.DoModal())
	{
		// if file opened successfully then set text of the edit control
		mEditFilePath.SetWindowTextW(fileDialog.m_szFileName);
		this->filePath = wide_char_to_string(fileDialog.m_szFileName);
		if (!filePath.empty())
		{
			// if the file path is not empty then enable the buttons
			mBtnUpdate.EnableWindow(TRUE);
			mBtnClear.EnableWindow(TRUE);
		}
		else
		{
			// if the file path is empty then disable the buttons
			mBtnClear.EnableWindow(FALSE);
			mBtnUpdate.EnableWindow(FALSE);
		}
	}
	return 0;
}

size_t CMainDlg::WriteDataToFile(const string &fileName, const string &data, bool append)
{
	fstream fs;
	if (append)
	{
		// open file for appending
		fs.open(fileName, ios::out | ios::app);
	}
	else
	{
		fs.open(fileName, ios::out);
	}
	fs << data;
	fs.close();
	return strlen(data.c_str());
}

LRESULT CMainDlg::OnBnClickedButtonClear(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	if (WriteDataToFile(filePath, "", false) == 0)
	{
		MessageBox(_T("清除成功"));
	}
	else
	{
		MessageBox(_T("清除失败"));
	}
	return 0;
}

// callback funciont for curl
size_t CMainDlg::WriteChunkData(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	return WriteDataToFile(filePath, ptr, true);
}

LRESULT CMainDlg::OnBnClickedButtonUpdate(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
{
	CURLcode result;
	char errBuffer[CURL_ERROR_SIZE];		// error buffer
	result = curl_easy_setopt(urlHander, CURLOPT_ERRORBUFFER, errBuffer);		// set error buffer
	if (CURLE_OK != result)
	{
		MessageBox(string_to_wide_char(errBuffer));
		return -1;
	}
	result = curl_easy_setopt(urlHander, CURLOPT_URL, url.c_str());		// set host file url
	if (CURLE_OK != result)
	{
		MessageBox(string_to_wide_char(errBuffer));
		return -1;
	}
	result = curl_easy_setopt(urlHander, CURLOPT_WRITEFUNCTION, &CMainDlg::WriteChunkData);		// set callback function
	if (CURLE_OK != result)
	{
		MessageBox(string_to_wide_char(errBuffer));
		return -1;
	}
	WriteDataToFile(filePath, "", false);			// remove the origin host file's content
	result = curl_easy_perform(urlHander);			// perform the operations
	if (CURLE_OK != result)
	{
		MessageBox(string_to_wide_char(errBuffer));
		return -1;
	}
	MessageBox(_T("更新成功"));
	return 0;
}
