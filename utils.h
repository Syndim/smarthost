#include "stdafx.h"
#include <Windows.h>
#include <string>
#include <sstream>
#include <vector>

#define BUFFER_SIZE 10240

LPCTSTR string_to_wide_char(const std::string &str);
std::string wide_char_to_string(LPCTSTR str);
std::string change_charset(const std::string &str, UINT from_charset, UINT to_charset);

bool file_exist(const std::string& file_path);
UINT count_number(UINT number);
std::string format_string(const std::string& format_str, const std::vector<std::string> &value_str);
bool mkdir(const std::string &file_path);
std::string replace(std::string str, const std::string &from, const std::string &to);
std::vector<std::string> split(const std::string &s, char delim);
